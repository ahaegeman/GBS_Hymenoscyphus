## GBS analysis of *Hymenoscyphus fraxineus* isolates

In this repository you can find all scripts used to analyse Genotyping-by-Sequencing (GBS) data from *Hymenoscyphus fraxineus* isolates.

The data analysis consists of 3 major parts, each with their own description:
 1) [preprocessing](https://gitlab.com/ahaegeman/GBS_Hymenoscyphus/-/blob/master/preprocessing.md)
 2) [reference-free analysis](https://gitlab.com/ahaegeman/GBS_Hymenoscyphus/-/blob/master/gibpss.md)
 3) [reference-based analysis](https://gitlab.com/ahaegeman/GBS_Hymenoscyphus/-/blob/master/mapping_variantcalling.md)

All scripts are distributed under the [MIT license](https://opensource.org/licenses/MIT).
