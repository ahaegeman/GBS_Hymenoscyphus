#!/bin/bash

date

echo "--------"
echo "Trimming"
echo "--------"

#########################################################################################################################################
# Author: Christophe Verwimp, P21, Growth & Development
# Adjusted by Annelies Haegeman for use with paired end reads.
# version: 7, adjusted for PE reads
# 26/01/2016
# This script trims reads in fastq files:
#			- trims 3' adapter sequences (incl. barcodes and restriction site remnants)
# 			- trims 5' restriction site remnants
#			- trims all reads to a maximum length
#			- trims intact internal restriction sites
#			- discards reads shorther than custom minimum
#
# This script makes use of:
#			- cutadapt
#			- fastx_trimmer
#			- trim_fastq_after_internal_restriction_site.py (author: Tom Ruttink)
#
# Usage:	bash GBS_Trimming_v2.sh TableOfBarcodes.lst minimumLength Sequencing(SE/PE) ReadLength Inputdirectory outputdirectory
#
# Example:	bash GBS_Trimming_v2.sh TableOfBarcodes.lst 25 S 100 reads/ trimmed_reads/
#
# This script is used in the GBS_Mnemiopsis workflow.
#
# Version changes (1 => 2):
# 	- additional argument for single (SE) or paired-end (PE) sequencing
#	- less TableOfBarcodes columns
#	- automatic determination of maximum ReadLength (readlength - maximal barcode length)
#
# Version changes (2 => 3):
#	- custom directories as argument
#	- adapted for quality filtering prior trimming (column numbers sample list)
#	- cutadapt overlap + 1
#
# Version changes (3 => 6):
#	- parallelization
#	- paired-end currently not working
#
# Version changes (6 => 7):
#	- paired end working again, but information on pairs is lost
# Version 7 => 7 PE
#	- changed so that PE reads can be processed as well.
#########################################################################################################################################

# function declaration

	function parallelizeTrimming {

		# General parameters.

			Samples=$(cat $TableOfBarcodes | grep -v "^#" | cut -f1)

			NumberOfSamples=$(cat $TableOfBarcodes | grep -v "^#" | wc -l)
			echo -e "\nNumber of samples = "$NumberOfSamples

			MaxBarcodeLength=$(cat $TableOfBarcodes | grep -v "^#" | cut -f2 | awk '{print length}' | sort -n | tail -1)	# find maximum barcode length
			echo "Maximal barcode length = "$MaxBarcodeLength

			MaximalReadPositionTrimmingforVariableBarcodes=$(( $ReadLength - $MaxBarcodeLength ))
			echo "Maximal position after trimming/compensating for variable barcode length = "$MaximalReadPositionTrimmingforVariableBarcodes

		# Create info table and start parallel.

			Counter=1

			for Sample in $Samples; do

				echo -e "$Counter"@"$DeleteIntermediateFiles\t$Sample\t$MinimumLength\t$Sequencing\t$MaximalReadPositionTrimmingforVariableBarcodes\t$InputDirectory\t$OutputDirectory\t$ScriptsDirectory\t$TableOfBarcodes"

				Counter=$(($Counter + 1))

				done | perl /usr/bin/parallel --jobs $MaxNumberOfCores --colsep '\t' -- trimming {1} {2} {3} {4} {5} {6} {7} {8} {9}

		}
		
	function trimming {

		# Accept parameters.

			Counter_DeleteIntermediateFiles=$1
			Sample=$2
			MinimumLength=$3
			Sequencing=$4
			MaximalReadPositionTrimmingforVariableBarcodes=$5
			InputDirectory=$6
			OutputDirectory=$7
			ScriptsDirectory=$8
			TableOfBarcodes=$9
			
			Counter=$(echo $Counter_DeleteIntermediateFiles | cut -f1 -d "@")
			DeleteIntermediateFiles=$(echo $Counter_DeleteIntermediateFiles | cut -f2 -d "@")

			echo -e "\n==> Counter = "$Counter

		# Retrieve per sample parameters from TableOfBarcodes.

			RestrictionEnzymeCombination=$(cat $TableOfBarcodes | grep -v "^#" | cut -f3,4 | tr '\t' '-' | head -n $Counter | tail -1)

			BarcodeSequence=$(cat $TableOfBarcodes | grep -v "^#" | cut -f2 | head -n $Counter | tail -1)

		# Hardcoded restriction site sequences (next version: harcoded => seperate file).

			echo -e "\nGetting RestrictionEnzyme and cutsites..."	

	 			echo -e "\nRestrictionEnzymeCombination = "$RestrictionEnzymeCombination

					if [[ $RestrictionEnzymeCombination = "ApeKI" ]]; 
					then
						CutsiteRemnantBarcodeSide=GCWG
						FullCutsiteBarcodeSide=GCTGC
						FullCutsiteBarcodeSide2=GCAGC

						CutsiteRemnantCommonSide=GCWG
						FullCutsiteCommonSide=GCTGC
						FullCutsiteCommonSide2=GCAGC

					elif [[ $RestrictionEnzymeCombination = "PstI" ]];
					then
						CutsiteRemnantBarcodeSide=CTGCA
						FullCutsiteBarcodeSide=CTGCAG

						CutsiteRemnantCommonSide=CTGCA
						FullCutsiteCommonSide=CTGCAG

					elif [[ $RestrictionEnzymeCombination = "MspI" ]];
					then
						CutsiteRemnantBarcodeSide=CCG
						FullCutsiteBarcodeSide=CCGG

						CutsiteRemnantCommonSide=CCG
						FullCutsiteCommonSide=CCGG

					elif [[ $RestrictionEnzymeCombination = "PstI-MspI" ]];
					then
						CutsiteRemnantBarcodeSide=CTGCA
						FullCutsiteBarcodeSide=CTGCAG

						CutsiteRemnantCommonSide=CCG
						FullCutsiteCommonSite=CCGG

	 				else
						echo "Cutsite not found!"
						exit 0

	 				fi

		# Hardcoded adapter sequences, = universal GBS adapter sequences (standard barcode-common or barcode-yshaped results in same sequences)
			# SEQUENCES INCLUDE PRIMER ADDITIONS / FLOWCELL FEET

			BarcodedAdapterSequence=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
			BarcodedAdapterSequenceRC=$(echo $BarcodedAdapterSequence | rev | tr GATC CTAG)
			CommonAdapterSequence=AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGCCGATCTCGTATGCCGTCTTCTGCTTG

			CutsiteRemnantLengthBarcodeSide=$(echo $CutsiteRemnantBarcodeSide | awk '{print length}')
			echo "Cutsite remnant length BarcodeSide = "$CutsiteRemnantLengthBarcodeSide
			OverlapBarcodeSide=$(($CutsiteRemnantLengthBarcodeSide + 1))
			echo "Minimum overlap BarcodeSide = "$OverlapBarcodeSide

			CutsiteRemnantLengthCommonSide=$(echo $CutsiteRemnantCommonSide | awk '{print length}')
			echo "Cutsite remnant length CommonSide = "$CutsiteRemnantLengthCommonSide
			OverlapCommonSide=$(($CutsiteRemnantLengthCommonSide + 1))
			echo "Minimum overlap CommonSide = "$OverlapCommonSide

				# Manage filenames.

					FastQ_Demultiplexed_F=$(echo $Sample"_F.fq")
					FastQ_Demultiplexed_R=$(echo $Sample"_R.fq")
					FastQ_InternalAdaptersTrimmed_F=$(echo $Sample"_F_InternalAdaptersTrimmed.fq")
					FastQ_InternalAdaptersTrimmed_R=$(echo $Sample"_R_InternalAdaptersTrimmed.fq")
					FastQ_ResRemAndTrimmed_F=$(echo $Sample"_F_ResRemAndTrimmed.fq")
					FastQ_ResRemAndTrimmed_R=$(echo $Sample"_R_ResRemAndTrimmed.fq")

			 		echo "FastQ_Demultiplexed_F = "$FastQ_Demultiplexed_F
			 		echo "FastQ_Demultiplexed_R = "$FastQ_Demultiplexed_R					
					echo "FastQ_InternalAdaptersTrimmed_F = "$FastQ_InternalAdaptersTrimmed_F
					echo "FastQ_InternalAdaptersTrimmed_R = "$FastQ_InternalAdaptersTrimmed_R
					echo "FastQ_ResRemAndTrimmed_F = "$FastQ_ResRemAndTrimmed_F
					echo "FastQ_ResRemAndTrimmed_R = "$FastQ_ResRemAndTrimmed_R

				# Clip internal adapters (and 3' restriction site remnant).

					# get direction, in case of forward reads the cutsite remnant is pasted to the CommonAdapterSequence adapter sequence
				 	#		 in case of reverse reads: cutsite remnant + reverse complement of barcode + reverse complement of BarcodedAdapterSequence adapter

					echo -e "\nClip internal adapter"
					
						Adapter_F=$(echo $CutsiteRemnantCommonSide$CommonAdapterSequence)
						Overlap_F=$OverlapCommonSide
						BarcodeSequenceRC=$(echo $BarcodeSequence | rev | tr GATC CTAG)
			 			echo "BarcodeSequence = "$BarcodeSequence
			 			echo "BarcodeSequenceRC = "$BarcodeSequenceRC
						Adapter_R=$(echo $CutsiteRemnantBarcodeSide$BarcodeSequenceRC$BarcodedAdapterSequenceRC)
									# this is cutsite remnant barcode side + RC van BarcodeSequence + RC van BarcodedAdapterSequence adapter sequentie
						Overlap_R=$OverlapBarcodeSide

						if [[ "$Overlap_F">"$Overlap_R" ]]; then	#take as overlap parameter for cutadapt the largest value of Overlap_F or Overlap_R
							Overlap_max=$Overlap_F
						else
							Overlap_max=$Overlap_R
						fi	
							
						echo "Adapter_F = "$Adapter_F
						echo "Adapter_R = "$Adapter_R
						echo "Overlap_max = "$Overlap_max

					 # incl cutsite, and possibly BarcodeSequence
									  # the entire cutsite remnant +1 should be present for trimming


					cutadapt -a $Adapter_F -A $Adapter_R --overlap $Overlap_max --minimum-length $MinimumLength -o $OutputDirectory/$FastQ_InternalAdaptersTrimmed_F -p $OutputDirectory/$FastQ_InternalAdaptersTrimmed_R $InputDirectory/$FastQ_Demultiplexed_F $InputDirectory/$FastQ_Demultiplexed_R
					#the minimum length filter is set here because otherwise you risk ending up with sequences shorter then 6 nucleotides, if the next step is done, then the sequence is empty and the pairs would be broken.
				# Clip 5' restriction site remnant and trim 3'.

					echo -e "\nClip 5' restriction site remnant and trim 3'"

				 	#F reads

						FirstBase2keep_F=$(($CutsiteRemnantLengthBarcodeSide + 1))
						Lasttbase2keep_F=$MaximalReadPositionTrimmingforVariableBarcodes

					#R reads
						FirstBase2keep_R=$(($CutsiteRemnantLengthCommonSide + 1))
						Lasttbase2keep_R=$MaximalReadPositionTrimmingforVariableBarcodes
					
				 	echo "First base to keep for F reads= "$FirstBase2keep_F
				 	echo "First base to keep for R reads= "$FirstBase2keep_R
					echo "Last base to keep for F reads= "$Lasttbase2keep_F		#you can use this in the fastx_trimmer command using option -l, but if you leave it empty, it just takes the whole read.
					echo "Last base to keep for R reads= "$Lasttbase2keep_R
					
					fastx_trimmer -f $FirstBase2keep_F -i $OutputDirectory/$FastQ_InternalAdaptersTrimmed_F -o $OutputDirectory/$FastQ_ResRemAndTrimmed_F
					fastx_trimmer -f $FirstBase2keep_R -i $OutputDirectory/$FastQ_InternalAdaptersTrimmed_R -o $OutputDirectory/$FastQ_ResRemAndTrimmed_R

				# Count reads.

					echo -e "\nCount reads"

					NumberOfReadsAfterInternalAdapters_F=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$FastQ_InternalAdaptersTrimmed_F)
					NumberOfReadsAfterInternalAdapters_R=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$FastQ_InternalAdaptersTrimmed_R)
					NumberOfReadsAfterRSandTrimmed_F=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$FastQ_ResRemAndTrimmed_F)
					NumberOfReadsAfterRSandTrimmed_R=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$FastQ_ResRemAndTrimmed_R)
					
					TableOfBarcodes_output=$(echo $(echo $TableOfBarcodes | cut -f1 -d ".")"_output.tbl")

					echo -e $Sample"\t"$NumberOfReadsAfterInternalAdapters_F"\t"$NumberOfReadsAfterRSandTrimmed_F"\t"$NumberOfReadsAfterInternalAdapters_R"\t"$NumberOfReadsAfterRSandTrimmed_R >> $TableOfBarcodes_output

					echo "Sample = "$Sample
					echo "NumberOfReadsAfterInternalAdapters_F = "$NumberOfReadsAfterInternalAdapters_F
					echo "NumberOfReadsAfterInternalAdapters_R = "$NumberOfReadsAfterInternalAdapters_R
					echo "NumberOfReadsAfterRSandTrimmed_F = "$NumberOfReadsAfterRSandTrimmed_F
					echo "NumberOfReadsAfterRSandTrimmed_R = "$NumberOfReadsAfterRSandTrimmed_R

					
					
				# Delete intermediate files
				
					if [[ $DeleteIntermediateFiles == "TRUE" ]]; then
						echo -e "\nDeleting intermediate files"
						rm $InputDirectory/$FastQ_QualityFiltered
						rm $OutputDirectory/$FastQ_InternalAdaptersTrimmed
						rm $OutputDirectory/$FastQ_ResRemAndTrimmed
					fi
		}

		export -f trimming


# Start

	# Accept arguments

		for ArgumentCounter in "$@" ;do

			case $ArgumentCounter in

				-l=*|--TableOfBarcodes=*)
				TableOfBarcodes="${ArgumentCounter#*=}"
				shift
				;;

				-sd=*|--ScriptsDirectory=*)
				ScriptsDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-id=*|--InputDirectory=*)
				InputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-od=*|--OutputDirectory=*)
				OutputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-pr=*|--Sequencing=*)
				Sequencing="${ArgumentCounter#*=}"
				shift
				;;

				-rl=*|--ReadLength=*)
				ReadLength="${ArgumentCounter#*=}"
				shift
				;;

				-ml=*|--MinimumLength=*)
				MinimumLength="${ArgumentCounter#*=}"
				shift
				;;

				-c=*|--MaxNumberOfCores=*)
				MaxNumberOfCores="${ArgumentCounter#*=}"
				shift
				;;

				-di=*|--DeleteIntermediateFiles=*)
				DeleteIntermediateFiles="${ArgumentCounter#*=}"
				shift
				;;

				esac
			done

		echo "pwd = "$(pwd)

		echo "TableOfBarcodes = "$TableOfBarcodes
		echo "ScriptsDirectory = "$ScriptsDirectory
		echo "InputDirectory = "$InputDirectory
		echo "OutputDirectory = "$OutputDirectory
		echo "Sequencing = "$Sequencing
		echo "ReadLength = "$ReadLength
		echo "MinimumLength = "$MinimumLength
		echo "MaxNumberOfCores = "$MaxNumberOfCores
		echo "DeleteIntermediateFiles = "$DeleteIntermediateFiles

	# Remove empty lines from TableOfBarcodes.
	
		sed -i '/^$/d' $TableOfBarcodes
		
	# Check DeleteIntermediateFiles
	
		if [[ $DeleteIntermediateFiles != "TRUE" ]] && [[ $DeleteIntermediateFiles != "FALSE" ]]; then
			echo -e "\nDeleteIntermediateFiles should be TRUE or FALSE."
			exit 0
		fi

	# Initiate output file

		TableOfBarcodes_output=$(echo $(echo $TableOfBarcodes | cut -f1 -d ".")"_Trimming_output.tbl")

		echo -e "#Sample\tNumberOfReadsAfterInternalAdapters_F\tNumberOfReadsAfterRSandTrimmed_F\tNumberOfReadsAfterInternalAdapters_R\tNumberOfReadsAfterRSandTrimmed_R"  > $TableOfBarcodes_output

	# Infer GBS protocol from TableOfBarcodes

		echo -e "\nInferring GBS protocol"

		EnzymeCombinations=$(cat $TableOfBarcodes | cut -f3,4 | sort | tr '\t' '-' | uniq)

		echo "EnzymeCombinations:"
		for EnzymeCombination in $EnzymeCombinations; do
			echo -e "\t"$EnzymeCombination
			done

		echo -e "Sequencing: \n\t"$Sequencing

	# Ask user if parameters are correct, and call parallelizeTrimming function

		#echo -e "\nAre these parameters correct (Y/N)?"; read Answer
		Answer="Y"

		if [[ $Answer == "N" ]];then

			echo -e "\nCheck arguments and input files."
			exit 0	

		elif [[ $Answer == "Y" ]];then

			parallelizeTrimming

		else

			echo -e "\nAnswer Y or N please"
			exit 0

		fi

	# Sort output on sample name, and print to screen.

		if [[ $Sequencing == "SE" ]]; then
			NumberOfFiles=$NumberOfSamples
		elif [[ $Sequencing == "PE" ]]; then
			NumberOfFiles=$(echo "scale=0; $NumberOfSamples * 2" | bc)
		fi

		echo "NumberOfFiles = "$NumberOfFiles
		(head -n 1 $TableOfBarcodes_output && tail -n $NumberOfFiles $TableOfBarcodes_output | sort) > temp; cat temp > $TableOfBarcodes_output; rm temp
		echo ""
		cat $TableOfBarcodes_output

date
