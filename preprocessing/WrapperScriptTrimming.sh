#!/bin/bash

####################################################################################################################################################################################################
# Author:	Christophe Verwimp, P21, Growth & Development
# Contact:	christophe.verwimp@ilvo.vlaanderen.be
# Date:		14/01/2016
#
####################################################################################################################################################################################################

# Arguments

	ScriptsDirectory="/home/genomics/kvanpoucke/Run17/Scripts/Scripts"
	LogDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/2_Trimming/Log"
	OutputDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/2_Trimming/Output"
	InputDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/1_Demultiplexing/Output"
	TableOfBarcodes="/home/genomics/kvanpoucke/Run17/Preprocessing/TableOfBarcodes.txt"
	Sequencing="PE"
	ReadLength=151
	MinimumLength=10
	MaxNumberOfCores=16
	DeleteIntermediateFiles="TRUE"

# Function declaration

	function trimming {

		bash $ScriptsDirectory/GBS_Trimming_v7_PE.sh \
			--TableOfBarcodes=$TableOfBarcodes \
			--ScriptsDirectory=$ScriptsDirectory \
			--InputDirectory=$InputDirectory \
			--OutputDirectory=$OutputDirectory \
			--Sequencing=$Sequencing \
			--ReadLength=$ReadLength \
			--MinimumLength=$MinimumLength \
			--MaxNumberOfCores=$MaxNumberOfCores \
			--DeleteIntermediateFiles=$DeleteIntermediateFiles
			
		}

# Call function and write output to log file.
		
	trimming 2>&1 | tee $LogDirectory/$(echo "Trimming_"$(date | sed 's/ /_/g')".log")
