#!/bin/bash

date
echo ""
echo "+----------------+"
echo "| Demultiplexing |"
echo "+----------------+"

####################################################################################################################################
# Author: Christophe Verwimp, P21, Growth & Development
# Contact: christophe.verwimp@ilvo.vlaanderen.be
# Version: 2
# Date: 08/01/2016
#
# Function:	Demultiplexing of FastQ file based on inline barcode sequences.
#		Barcode sequence is removed from read.
#		Number of mismatches allowed for barcode recognition
#
# Required:
#			- Python 2.7.6
#			- GBSX v1.0
#			- FastQ_CountReads.py
#
# Version changes (1 => 2):	- PE now possible
#
####################################################################################################################################

# Function declaration

	function checkTableOfBarcodes {
	
		# Create new filename.
		
			TableOfBarcodes_new=$(echo $(echo $TableOfBarcodes | cut -d "." -f1)"_new.tbl")

		# Remove empty lines.
	
			cat $TableOfBarcodes | sed '/^$/d' > temp
			
		# Remove header
		
			cat temp | grep -v "^#" > temp2

		# Order on descending barcode lentgh.
		
			cat temp2 | awk '{print length($2)"\t"$0}' | sort -nr | cut -f2- > $TableOfBarcodes_new
						
			rm temp temp2

		}

	function demultiplexer {
	
		# In case of PE, add reverse data argument

			if [[ $Sequencing="PE" ]]; then
				F2=$(echo "-f2 "$RawDataReverse)
			else
				F2=""
			fi

		# Call GBSX

			echo -e "\nCalling GBSX demultiplexer...\n"

			java -jar /usr/local/bin/GBSX_v1.3.jar --Demultiplexer \
		                                                    -f1 $RawDataForward $F2 \
		                                                    -i $TableOfBarcodes_new \
		                                                    -mb $NumberOfMismatchesAllowed \
		                                                    -me 0 \
		                                                    -gzip false \
		                                                    -o $OutputDirectory \
		                                                    -rad false \
		                                                    -kc true \
		                                                    -ca UUUUUUUUUUUUUUUUUUUUUUUUU # Prevent GBSX from trimming adpaters.
		
		}

	function renameAndGetNumberOfreadsPerSample {
	
		# Convert sequencing into list of directions.

			if [[ $Sequencing == "SE" ]]; then
				Directions="F"
			elif [[ $Sequencing == "PE" ]]; then
				Directions=$(echo -e "F\nR")
			fi
			
		# Get sample names
		
			Samples=$(cat $TableOfBarcodes_new | cut -f1)

		# Loop over directions

			for Direction in $Directions; do

				echo -e "\nDirection = "$Direction"\n"
			
				# Loop over samples, count reads, and delete if needed.
				
					# Determined reads

						Table_NumberOfReads=$(echo $(echo $RawDataForward | rev | cut -d "/" -f1 | rev | cut -d "." -f1)"_output.tbl")
					
						echo -e "#Sample\tNumberOfReads" > $Table_NumberOfReads
						
						if [ $Direction == "F" ] ; then
							R="R1"
						elif [ $Direction == "R" ] ; then
							R="R2"
						fi

						for Sample in $Samples; do

							mv $OutputDirectory/$Sample.$R.fastq $OutputDirectory/$Sample"_"$Direction".fq"

							NumberOfReads=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$Sample"_"$Direction".fq")

							echo -e $Sample"\t"$NumberOfReads
							echo -e $Sample"\t"$NumberOfReads >> $Table_NumberOfReads

							done

					# Undetermined reads

						mv $OutputDirectory/undetermined.$R.fastq $OutputDirectory/Undetermined_$Direction.fq

						NumberOfReads=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/Undetermined_F.fq)

						echo -e "Undetermined_F\t"$NumberOfReads
						echo -e "Undetermined_F\t"$NumberOfReads >> $Table_NumberOfReads
						
				done
					
		# Sort output on sample name and direction

			cat $Table_NumberOfReads | sort > temp; cat temp > $Table_NumberOfReads; rm temp

		}

# Start

	# Accept arguments

		for ArgumentCounter in "$@" ;do

			case $ArgumentCounter in

				-rdf=*|--RawDataForward=*)
				RawDataForward="${ArgumentCounter#*=}"
				shift
				;;

				-rdr=*|--RawDataReverse=*)
				RawDataReverse="${ArgumentCounter#*=}"
				shift
				;;
			
				-tb=*|--TableOfBarcodes=*)
				TableOfBarcodes="${ArgumentCounter#*=}"
				shift
				;;

				-mm=*|--NumberOfMismatchesAllowed=*)
				NumberOfMismatchesAllowed="${ArgumentCounter#*=}"
				shift
				;;

				-sd=*|--ScriptsDirectory=*)
				ScriptsDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-od=*|--OutputDirectory=*)
				OutputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-ld=*|--LogDirectory=*)
				LogDirectory="${ArgumentCounter#*=}"
				shift
				;;
					
				esac
			done

		echo "pwd = "$(pwd)

		echo "RawDataForward = "$RawDataForward
		echo "RawDataReverse = "$RawDataReverse
		echo "TableOfBarcodes = "$TableOfBarcodes
		echo "NumberOfMismatchesAllowed = "$NumberOfMismatchesAllowed
		echo "ScriptsDirectory = "$ScriptsDirectory
		echo "OutputDirectory = "$OutputDirectory
		echo "LogDirectory = "$LogDirectory
		
	# Check TableOfBarcodes

		echo -e "\nRemoving emty lines from TableOfBarcodes and ordering on descending barcode length"
		
		checkTableOfBarcodes
		
	# Infer GBS protocol from TableOfBarcodes

		echo -e "\nInferring GBS protocol"

		EnzymeCombinations=$(cat $TableOfBarcodes_new | cut -f3,4 | sort | tr '\t' '-' | uniq)

		echo "EnzymeCombinations:"
		for EnzymeCombination in $EnzymeCombinations; do
			echo -e "\t"$EnzymeCombination
			done

		if [[ $RawDataReverse != "" ]]; then
			Sequencing="PE"
		elif [[ $RawDataReverse == "" ]]; then
			Sequencing="SE"
		fi

		echo -e "Sequencing: \n\t"$Sequencing

	# Ask user if parameters are correct, and call demultiplexer and renameAndGetNumberOfreadsPerSample functions
		#echo -e "\nAre these parameters correct (Y/N)?"; read Answer
		#Do not ask user anymore of the parameters are correct but put answer directly to YES:
		Answer="Y"
		if [[ $Answer == "N" ]];then

			echo -e "\nCheck arguments and input files."
			exit 0	

		elif [[ $Answer == "Y" ]];then

			demultiplexer

			renameAndGetNumberOfreadsPerSample

		else

			echo -e "\nAnswer Y or N please"
			exit 0

		fi
		
date
