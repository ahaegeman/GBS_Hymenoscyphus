from Bio import SeqIO
import sys

def trim_restriction_site(records, restriction_site, min_len):
    """Trims after restriction site, checks read length.

    This is a generator function, the records argument should
    be a list or iterator returning SeqRecord objects.
    """
    len_restriction_site = len(restriction_site) #cache this for later
    for record in records:
        len_record = len(record) #cache this for later
        if len(record) < min_len:
           #Too short to keep
           continue
        index = record.seq.find(restriction_site)
        if index == -1:
            #adaptor not found, so won't trim
            yield record
        elif index >= min_len:
            #after trimming this will still be long enough
            yield record[0:index]

# take arguments from input
inputfile = sys.argv[1]
restriction_site = sys.argv[2]
min_len = int(sys.argv[3])
outputfile = sys.argv[4]

# load records from fastq file into SeqObjects

original_reads = SeqIO.parse(inputfile, "fastq")
trimmed_reads = trim_restriction_site(original_reads, restriction_site, min_len)

# rename the fasta outputfile based on the input filename: add _RE_trimmed
#input_name = inputfile.split('.')
#fasta_outputfile = str(input_name[0]) + "_RE_trimmed.fq"

count = SeqIO.write(trimmed_reads, outputfile, "fastq")

print "Saved %i reads" % count
