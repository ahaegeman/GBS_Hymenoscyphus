#!/bin/bash

####################################################################################################################################################################################################
# Author:	Christophe Verwimp, P21, Growth & Development, adjusted by Annelies Haegeman for PE GBS workflow
# Contact:	christophe.verwimp@ilvo.vlaanderen.be, annelies.haegeman@ilvo.vlaanderen.be
# Date:		27/01/2016
#
# This script Discards reads containing a maximum defined number of Ns.
#		- Filters reads on a minimum quality score for a minimum percentages of bases.
#		- Creates output table containing sample info: quality encoding, number of reads after discarding N's and number of reads after quality filtering.
#Version 5 of this script uses output of trimmed and merged reads from PE GBS. The merged reads are combined with unassembled forward and reverse reads in this step.
#It does NOT include automatic detection of Quality Encoding nor does it convert everything to Sanger format.
#
# Procedure:	
# 				
#				3. Open a terminal, navigate to the Demultiplexing directory and execute following command:
#
#						bash WrapperScriptDemultiplexing.sh
#
#				4. A new file is created containing the sample names and the numbers of reads per sample after demultiplexing.
#
# This script requires:
#
#			- Python 2.7.6
#			- GBS_QualityFiltering_v7.sh					in ScriptsDirectory
#			- parallel							absolut path embeded in script, adjust this if needed
#			- FastQ_CountReads.py 						in ScriptsDirectory
#			- prinseq-lite-0.20.4						absolut path embeded in script, adjust this if needed
#
# Notes:
#			- Raw 
#
####################################################################################################################################################################################################

# Arguments

	ScriptsDirectory="/home/genomics/kvanpoucke/Run17/Scripts/Scripts"
	OutputDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/4_QualityFiltering/Output"
	LogDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/4_QualityFiltering/Log"
	InputDirectory="/home/genomics/kvanpoucke/Run17/Preprocessing/3_Merging/Output"
	ListOfSamples="/home/genomics/kvanpoucke/Run17/Preprocessing/ListOfSamples.txt"
	MinMeanQuality=25
	MinLength=30
	MinBaseQuality=1	#A base with a lower quality is converted into an N
	MaxNumberOfNs=10 #Max 10 percent of the bases in a read are allowed to be N's
	MaxNumberOfCores=16
	TrimmingLengthOfNonMergedReads=30	#30 bases are clipped off the non merged reads (because the quality in the end of the read is low)

# Function declaration

	function qualityFiltering {

		bash $ScriptsDirectory/GBS_QualityFiltering_v7.sh \
			--ListOfSamples=$ListOfSamples \
			--ScriptsDirectory=$ScriptsDirectory \
			--LogDirectory=$LogDirectory \
			--InputDirectory=$InputDirectory \
			--OutputDirectory=$OutputDirectory \
			--MinMeanQuality=$MinMeanQuality \
			--MaxNumberOfCores=$MaxNumberOfCores \
			--MinLength=$MinLength \
			--MaxNumberOfNs=$MaxNumberOfNs \
			--MinBaseQuality=$MinBaseQuality \
			--TrimmingLengthOfNonMergedReads=$TrimmingLengthOfNonMergedReads
		}
		
# activate virtual environment for OBItools
source /home/genomics/ffocquet/.obitools_venv/bin/activate

# Call function and write output to log file.
	qualityFiltering 2>&1 | tee $LogDirectory/$(echo "QualityFiltering_"$(date | sed 's/ /_/g')".log")

#deactivate virtual environment
deactivate