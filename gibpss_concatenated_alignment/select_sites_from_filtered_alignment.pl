#!/usr/bin/perl

$|=1;
use strict;
#use warnings;
use Bio::AlignIO;
use Bio::SeqIO;
use Getopt::Long;
use Pod::Usage;
use List::Util qw( min max );

########
#manual#
########
=head1 NAME

select_sites_from_filtered_alignment.pl - make a new alignment from a filtered alignment from selected sites only

=head1 SYNOPSIS

select_sites_from_filtered_alignment.pl  -input | i <filename> -pos | p <filename> -sites | s <filename> -output | o <filename>

=head1 DESCRIPTION

select_sites_from_filtered_alignment.pl will further filter a concatenated alignment based on a list of positions to keep. 
It also reads the "info_positions" file from the concatenated alignment.
The output is a filtered fasta file and a new "info_positions_selected" file containing only the sites kept.

=head1 COMMAND LINE OPTIONS AND ARGUMENTS

Description of options

=head2 Mandatory arguments

=over

=item -input | i <filename>

The input file is the fasta file of the filtered concatenated alignment as it is produced by the script "convert_genotypes_to_concatenated_alignment_and_filter.pl".

=item -pos | p <filename>

The position file is the "info_positions_filtered_alignment.txt" file as it is produced by the script "convert_genotypes_to_concatenated_alignment_and_filter.pl".

=item -sites | s <filename>

The sites file is a file with the positions of interest, one position number per line.

=item -output | o <filename>

The file name you choose as output file. The output file is a fasta file of the filtered concatenated alignment.

=back

=head1 BUG REPORTS & COMMENTS

Please send an e-mail to annelies.haegeman@ilvo.vlaanderen.be

=cut

# print manual if no options given
if (!$ARGV[0]) {
	print STDERR `pod2text $0`;
	exit;
}

#############
#input files#
#############
# parse command line
my ($datafile, $outputfile,$posdatafile,$locdatafile);
GetOptions	(
				'input|i=s' => \$datafile,			#s = string
				'sites|s=s' => \$locdatafile,
				'pos|p=s' => \$posdatafile,
				'output|o=s' => \$outputfile,
			 	'<>' => \&other_options
			);

# check for mandatory options
pod2usage( { -message => q{One or more mandatory argument is missing!} ,
             -exitval => 1  ,
             -verbose => 1
           } ) unless ($datafile and $outputfile and $posdatafile and $locdatafile);

#####################
#initiate variabeles#
#####################
my($seqio1,$sequence_object1,$id,$seq,$lijn,@sites,@posinfo,@keptsites,@arrayseq,$pos, $origpos, $newpos);
open(OP1,">$outputfile");
open(OP2,">info_positions_filtered_alignment_after_sitefiltering.txt");
print OP2 "position_filtered_alignment\tposition_unfiltered_alignment\tposition_original_unfiltered_alignment\tpoplocID\tvarpos\tlocuslength\tnSNP\tn_all\tnInd\tcons\n";

##################################################
#read sites file and store site positions in array
##################################################
open (IP1,$locdatafile) || die "cannot open \"$locdatafile\":$!";

while ($lijn=<IP1>){
	chomp $lijn;
	push(@sites,$lijn);
}
close(IP1);


#############################################################
#read positions info file and save only positions of interest
#############################################################
open (IP2, $posdatafile) || die "cannot open \"$posdatafile\" :$!";
$origpos=0; #original position, 0-based count
$newpos=0;	#new position in filtered alignment, 1-based count when printed to output
while ($lijn=<IP2>){
	chomp $lijn;
	next if $. < 2; # Skip this loop when running through the first line
	@posinfo = split (/\t/,$lijn);	#split line on tabs, the fields are: position_filtered_alignment	position_unfiltered_alignment	poplocID	varpos	locuslength	nSNP	n_all	nInd	cons
	if (grep { $_ eq $posinfo[0] } @sites) {		#check if locus number of the line is in the array of loci to keep
		$newpos++; #new position in filtered alignment, do +1 to start with position nr 1 in final filtered alignment
		print OP2 "$newpos\t";
		print OP2 "$lijn\n";
		push(@keptsites,$posinfo[0]); #keep array with the sites which are retained (1-based positions)
		#print "$posinfo[2]\t$posinfo[0]\t$newpos\n";
	}
	$origpos++; #when going to the next line, do +1 to original position
}
close(IP2);

	
###############################################################
#read fasta file and print only positions of interest to output
###############################################################
$seqio1 = Bio::SeqIO -> new('-format' => 'fasta','-file' => $datafile);
while ($sequence_object1 = $seqio1 -> next_seq) {
	$id = $sequence_object1->id();
	$seq = $sequence_object1->seq();
	print OP1 ">$id\n";
	@arrayseq=split(undef,$seq); #make an array of the sequence string
	foreach $pos (@keptsites){ #only print the positions of interest (1-based positions)
	  print OP1 $arrayseq[$pos-1];	#1-based positions, hence we need to substract 1 to get the correct position
	}
	print OP1 "\n"; #print newline character at the end of the sequence
}

print "Finished and saved $newpos sites.\n";