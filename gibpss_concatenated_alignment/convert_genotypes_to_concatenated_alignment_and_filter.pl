#!/usr/bin/perl

$|=1;
use strict;
#use warnings;
use Bio::AlignIO;
use Bio::SeqIO;
use Getopt::Long;
use Pod::Usage;
use List::Util qw( min max );

########
#manual#
########
=head1 NAME

convert_genotypes_to_concatenated_alignment_and_filter.pl - converts the Gibpss genotypes.txt file to a filtered concatenated alignment

=head1 SYNOPSIS

convert_genotypes_to_concatenated_alignment_and_filter.pl  -input | i <filename> -loc | l <filename> -gap | g <integer> -single | s <string> -output | o <filename>

=head1 DESCRIPTION

convert_genotypes_to_concatenated_alignment_and_filter.pl will convert the Gibpss genotypes.txt file to a concatenated alignment in fasta format.
It will do an additional filtering step to remove:
	1) invariant positions (positions which have the same base across all individuals, gaps are considered as a base);
	2) positions with maximum <g> gaps, i.e. maximum <g> individuals with missing data
If "single or s" is set to "yes", then an extra filtering step is done:
	3) positions where only 1 individual has an aberrant base, this is probably a random error (gaps are considered as a base);
The script also takes the locdata.txt file from Gibpss as input to keep track of which loci and bases are kept in the final alignment.
An output file containing this information (called "info_positions.txt") is generated automatically when running the script.

=head1 COMMAND LINE OPTIONS AND ARGUMENTS

Description of options

=head2 Mandatory arguments

=over

=item -input | i <filename>

The input file is the "genotypes.txt" file as it is produced by the software Gibpss.

=item -loc | l <filename>

The loc file is the "locdata.txt" file as it is produced by the software Gibpss.

=item -gap | g <integer>

Maximum number of gaps which are allowed to keep the position in the alignment, i.e. maximum number of individuals with missing data.
If put at 0, no missing data is allowed. If put at the number of individuals in the dataset, all positions are allowed regardless of the number of missing data.

=item -single | s <filename>

If this option is set to "yes", an extra filtering step is done to remove all positions where there is only 1 aberrant base (1 sample with deviating base).

=item -output | o <filename>

The file name you choose as output file. The output file is a fasta file of the filtered concatenated alignment.

=back

=head1 BUG REPORTS & COMMENTS

Please send an e-mail to annelies.haegeman@ilvo.vlaanderen.be

=cut

# print manual if no options given
if (!$ARGV[0]) {
	print STDERR `pod2text $0`;
	exit;
}

#############
#input files#
#############
# parse command line
my ($datafile, $outputfile,$locdatafile,$gap,$single);
GetOptions	(
				'input|i=s' => \$datafile,			#s = string
				'loc|l=s' => \$locdatafile,
				'gap|g=i' => \$gap,
				'single|s=s' => \$single,
				'output|o=s' => \$outputfile,
			 	'<>' => \&other_options
			);

# check for mandatory options
pod2usage( { -message => q{One or more mandatory argument is missing!} ,
             -exitval => 1  ,
             -verbose => 1
           } ) unless ($datafile and $outputfile and $gap and $locdatafile);
		   
#set the $single parameter according to the parsed options read from the command
if ((defined $single) && ($single eq "yes")) {
    $single="yes";
} else {
    $single="no";
}

#print "single: $single\n";

#####################
#initiate variabeles#
#####################
my ($lijn,@line,@names,$length,@elementlist,%seqs,$empty,$id,@locinfo,%sl,%cons,%nSNP,%varpos,%nall,%nInd,$poplocID,$position,@vars,$numbers);
my ($sequence_object1,$seqio1,$seq,@seqarray,$length,$element,%count,@freqs,$min,$max,@finalpositions,$keep,$id,@arrayseq,$pos,$k,$v,$origpos,$newpos,@keptloci,%counts,@unique,$nrloci);
my $numseq=0;
my $numberofgaps=0;
my $string="";
my $i=0;
my $j="-999";
my $k=0;
my $l=0;
my $totallength=0;
my $totalloci=0;
open(OP, ">concatenated_alignment_unfiltered.fasta");
open(OP2,">info_positions_unfiltered_alignment.txt");
print OP2 "position_unfiltered_alignment\tpoplocID\tvarpos\tlocuslength\tnSNP\tn_all\tnInd\tcons\n";
open(OP3,">$outputfile");
open(OP4,">info_positions_filtered_alignment.txt");
print OP4 "position_filtered_alignment\tposition_unfiltered_alignment\tpoplocID\tvarpos\tlocuslength\tnSNP\tn_all\tnInd\tcons\n";

##############################################################################
#SCRIPT PART1: CONSTRUCT UNFILTERED CONCATENATED ALIGNMENT FROM GIBPSS OUTPUT#
##############################################################################

print "\nConverting genotypes file to concatenated alignment....\n\n";

#read locdatafile and store information in a hash with as key the poplocID and as value the information of interest
open (IP,$locdatafile) || die "cannot open \"$locdatafile\":$!";
while ($lijn=<IP>)
	{	chomp $lijn;
		next if $. < 2; # Skip first line
		#poplocID	sl	cons	nSNP	varpos	n_all	nInd	nIndloc	ties
		@locinfo = split (/\t/,$lijn); #split line into fields
		$sl{$locinfo[0]}=$locinfo[1];		#store relevant info in hashes with as key the poplocID
		$cons{$locinfo[0]}=$locinfo[2];
		$nSNP{$locinfo[0]}=$locinfo[3];
		$varpos{$locinfo[0]}=$locinfo[4];
		$nall{$locinfo[0]}=$locinfo[5];
		$nInd{$locinfo[0]}=$locinfo[6];
		#print "$locinfo[0]\t";
		#print "$nSNP{$locinfo[0]}\n";
	}
close(IP);

#read genotypes file and loop over all lines (=loci)
open (IP2,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP2>)
	{	chomp $lijn;
		if ($. == 1){		#store names of isolates in first line
			@names = split (/\t/,$lijn);
			shift(@names); #remove the first element (which is "poplocID")
		}
		else{	#from second line on, make a file per locus containing the sequences from all samples

			#prepare data
			@line = split (/\t/,$lijn);
			$poplocID=$line[0];  #store poplocID in $poplocID
			shift(@line); #remove the first element (which is the locus name, the locus name was stored in $poplocID)

			#check if the locus had SNPs, and only continue if SNPs were present (nSNP>0)
			if ($nSNP{$poplocID}>0){
				#keep track of the number of loci which contain SNPs
				$totalloci++;
				#determine the total length of the SNPs of the locus by looping over all elements of a line, and when the element is different than -999, save the length
				$length=$nSNP{$poplocID};
				#print "$poplocID\t";
				#print "$length\n";

				#loop over the elements (=sequences) in the line and add to hash with as ID the sample name and as value the previous value with the new value concatenated
				foreach my $element (@line){
					if ($element eq "consensus"){

					}
					elsif ($element ne "-999") {
						@elementlist = split (/\//,$element);
						if (scalar(@elementlist) == 1) {	#if only 1 allele
							$seqs{$names[$i]}=$seqs{$names[$i]}.$elementlist[0];	#hash with as key the name of the sample and as value the concatenated sequence
							}
						else {	#if more than 1 allele
							#open TEMP file to store sequence data of the alleles in fasta format
							open (TEMP,">temp.fasta");
							foreach my $allele (@elementlist){
								print TEMP ">$k\n";
								print TEMP "$allele\n";
								$k++;
							}
							close(TEMP);

							#now calculate consensus sequence from alignment
							my $consensus = lc(consensus("temp.fasta"));

							#write consensus sequence to hash
							$seqs{$names[$i]}=$seqs{$names[$i]}.$consensus;

							#initiate variables, make TEMP file empty
							$k=0;
							system("rm temp.fasta");
						}
					}
					else {	#if -999, add "-" for the length of the locus
						#print OP ">$names[$i]\n";
						$empty="-" x $length;
						#print "$empty\n";
						$seqs{$names[$i]}=$seqs{$names[$i]}.$empty;
					}
					$i++;	#counter variable
				}

				#keep track of the positions: which position is derived from which locus? Write this info to a new output file, including some extra info on the locus itself
				#first put the elements of varpos "p:30/31" of the specific locus in a separate list
				$numbers=substr($varpos{$poplocID},2);	#take the variable positions of the locus, and remove the first two characters ("p:")
				@vars = split (/\//,$numbers);
				#next loop over all positions in the locus and write relevant info to an output txt file
				while ($l<$length){
					$position=$totallength+$l+1; 	#position in alignment = total length of the alignment so far + current base in the loop (=position in current locus) + index 1
					print OP2 "$position\t";
					print OP2 "$poplocID\t";
					print OP2 "$vars[$l]\t";	#extract the correct position from the @vars list
					print OP2 "$sl{$poplocID}\t";
					print OP2 "$nSNP{$poplocID}\t";
					print OP2 "$nall{$poplocID}\t";
					print OP2 "$nInd{$poplocID}\t";
					print OP2 "$cons{$poplocID}\n";
					#go to next position in alignment
					$l++;
					}


				#calculate the total length of the alignment so far
				$totallength=$totallength + $length; #length of the alignment so far + length of the last added locus

				#after finishing processing the line, but the counters back at zero
				$i=0;
				$l=0;
			}
		}
	}

close(IP2);

#when finishing reading the data, print the resulting hash %seqs to a fasta formatted file
foreach $id (keys %seqs) {
    print OP ">$id\n";
    print OP "$seqs{$id}\n";
}

#Report that the script has finished, and some metrics
print "\nFinished making the unfiltered concatenated alignment.\n";
print "The unfiltered alignment is $totallength bases long and is derived from $totalloci different loci.\n\n";
print "Now filtering the alignment by removing invariant positions, positions with only 1 aberrant individual, and positions with more than $gap gaps........\n\n";


#############################################
#SCRIPT PART2: FILTER CONCATENATED ALIGNMENT#
#############################################

########################################
#read fasta file and store all sequences
########################################
$seqio1 = Bio::SeqIO -> new('-format' => 'fasta','-file' => "concatenated_alignment_unfiltered.fasta");
while ($sequence_object1 = $seqio1 -> next_seq) {
	$seq = $sequence_object1->seq();
  push(@seqarray,$seq);
  $length = length($seq);
  $numseq++;  #total number of sequences
}

##########################################################
#loop over sequences position-wise, decide to keep or remove
##########################################################

while ($i < $length){ #start loop for all positions of the alignment
	#create a string for that position
	foreach $element (@seqarray){ #loop over all sequences in the @seqarray and store the string for the position of interest ($i)
		$string=$string.substr($element,$i,1);
		#print "$i\t$string\n";
	}
	#for the created string, calculate the frequencies and store in hash %count
	foreach my $char (split undef, $string) {
		#print "$char\n";
		if ($char eq "-"){  #store gaps in the hash with "gap" as key
			$count{gap}++;
			$numberofgaps++;
		}
		else{
			$count{$char}++;  #hash %count with as key the character and as value the number of times it occurs
		}
	}
	#if no gaps in the string, put the hash with gap at zero
	if ($numberofgaps == 0){
		$count{gap}=0;
	}
	#loop over the created hash at a certain position, calculate the frequencies and store them in an array
	while ( ($k,$v) = each %count ) {
		push(@freqs,$v/$numseq*100); #add all frequencies of the bases at a certain position to the array @freqs
		#print "position $i\t$k => $v\n";
		#print "position $i\t number of gaps $count{gap}\n";
	}
	#check the minimum / maximum frequencies and decide whether or not to keep the position
	$min = min @freqs;
	$max = max @freqs;
	if ($single eq "yes"){	#in case we also want the positions filtered out which occur in only 1 individual
		#print "single is yes\n";
		if (($min != 100) && ($max != ($numseq-1)*100/$numseq) && ($count{gap}<=$gap)){ #if the minimum is 100, it means that there is no variation at all at this position. If the maximum is (number of samples - 1)/(number of samples)*100, it means that only 1 sample is deviating, hence this is not informative either (probably a random error)
			push(@finalpositions,$i); #store the positions (0-based) to keep in a separate array
			$keep="yes";
		}
		else{
			$keep="no";
		}
	}
	else {	#in case we do not want the positions filtered out which occur in only 1 individual
		#print "single is no\n";
		if (($min != 100) && ($count{gap}<=$gap)){ #if the minimum is 100, it means that there is no variation at all at this position. If the maximum is (number of samples - 1)/(number of samples)*100, it means that only 1 sample is deviating, hence this is not informative either (probably a random error)
			push(@finalpositions,$i); #store the positions (0-based) to keep in a separate array
			$keep="yes";
		}
		else{
			$keep="no";
		}
	}
	#print "$i\t$count{gap}\t$min\t$max\t$keep\n";
	#cleanup and finish loop
	undef $string; #empty the string before you go to the next position
	undef %count; #empty the hash
	undef @freqs; #empty the array with frequencies
	$numberofgaps =0; #put number of gaps back at 0
	$i++; #go to next iteration (next position in alignment) by increasing $i by 1
}

#####################################################################
#read fasta file again and print only positions of interest to output
#####################################################################
$seqio1 = Bio::SeqIO -> new('-format' => 'fasta','-file' => "concatenated_alignment_unfiltered.fasta");
while ($sequence_object1 = $seqio1 -> next_seq) {
	$id = $sequence_object1->id();
	$seq = $sequence_object1->seq();
	print OP3 ">$id\n";
	@arrayseq=split(undef,$seq); #make an array of the sequence string
	foreach $pos (@finalpositions){ #only print the positions of interest (0-based positions)
	  print OP3 $arrayseq[$pos];
	}
	print OP3 "\n"; #print newline character at the end of the sequence
}

########################################################################
#read positions info file and print only positions of interest to output
########################################################################
open (IP3,"info_positions_unfiltered_alignment.txt") || die "cannot open \"info_positions_unfiltered_alignment.txt\":$!";
$origpos=0; #original position, 0-based count
$newpos=0;	#new position in filtered alignment, 1-based count when printed to output
while ($lijn=<IP3>){
	chomp $lijn;
	next if $. < 2; # Skip this loop when running through the first line
	#check if the original position of the unfiltered alignment is in the array of positions to keep, if so, print locus information to output
	if (grep $_ eq $origpos, @finalpositions) {
		$newpos++; #new position in filtered alignment, do +1 to start with position nr 1 in final filtered alignment
		print OP4 "$newpos\t";
		print OP4 "$lijn\n";
		#also store the loci numbers in an array to be able to calculate the total number of loci kept after filtering.
		@locinfo = split (/\t/,$lijn);
		push(@keptloci,$locinfo[1]); #keep array with the poplocIDs which are retained
	}
	$origpos++; #when going to the next line, do +1 to original position
}
close(IP3);

#Report that the script has finished, and some metrics
#calculate the number of different loci
$counts{$_}++ for @keptloci;
@unique = keys(%counts);
$nrloci = @unique; #use array in scalar context to get the number of elements in the array

print "Finished making the filtered concatenated alignment.\n";
print "The filtered alignment is $newpos bases long and is derived from $nrloci different loci.\n\n";



########################################################################
#FUNCTIONS

#function "consensus" with as argument a fasta file with aligned sequences, the function returns the consensus sequence with iupac code
sub consensus {
	my $seqname = "consensus";
	my $in  = Bio::AlignIO->new(-file   => @_ ,
                          -format => 'fasta');
	my $ali = $in->next_aln();
	my $iupac_consensus = $ali->consensus_iupac();   # dna/rna alignments only
	my $seq = Bio::Seq->new(-seq => "$iupac_consensus",
                        -display_id => $seqname);
	my $seq2 = $seq->seq;
	return $seq2;
}
