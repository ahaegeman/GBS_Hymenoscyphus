# Preprocessing

This repository contains scripts for the preprocessing of Genotyping-by-Sequencing data from *Hymenoscyphus fraxineus* isolates.
Important remark: the scripts were made for our local server and may need some updating before they will run on your system (for example absolute paths to external software should be updated).

All scripts are distributed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation requirements

The following software was used to develop and run the scripts in this repository (newer/older versions of the software might work as well, but this was not tested).

* Operating system: Ubuntu 18.04
* [perl](https://www.perl.org/) 5.26.1
* [Bioperl](https://bioperl.org/) 1.7.7
* [GNU Parallel](https://www.gnu.org/software/parallel/) v20161222 (should be in $PATH, it is called in the scripts as `parallel`)
* [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html) 2.8 (should be in $PATH, it is called in the scripts as `cutadapt`)
* [FASTX Toolkit](http://hannonlab.cshl.edu/fastx_toolkit/) 0.0.14 (all programs should be in $PATH, it is called in the scripts as `fastx_trimmer` and `fastq_masker`)
* [python](https://www.python.org/) 2.7 (should be in $PATH, it is called in the scripts as `python`)
* Python packages for python 2.7: [Biopython](https://biopython.org/) 1.65
* [python](https://www.python.org/) 3.6.9 (should be in $PATH, it is called in the scripts as `python3`)
* Python packages for python 3.6.9: [os](https://docs.python.org/3/library/os.html), [sys](https://docs.python.org/2/library/sys.html), [argparse](https://docs.python.org/3/library/argparse.html), [datetime](https://docs.python.org/2/library/datetime.html), [multiprocessing](https://docs.python.org/2/library/multiprocessing.html), [pandas](https://pandas.pydata.org/), [shelve](https://docs.python.org/3/library/shelve.html)
* [PEAR](https://cme.h-its.org/exelixis/web/software/pear/) 0.9.11 (should be in $PATH, it is called in the scripts as `pear`)
* [GBSX](https://github.com/GenomicsCoreLeuven/GBSX) 1.3.0 (note that in the Demultiplexing script, you will need to adjust the path to GBSX to your local path)
* [prinseq-lite](https://github.com/b-brankovics/grabb/blob/master/docker/prinseq-lite.pl) 0.20.4 (should be in $PATH, it is called in the scripts as `prinseq-lite`)
* [obitools](https://pythonhosted.org/OBITools/welcome.html) 1.01 22 (should be in $PATH, it is called in the scripts as `obigrep`)
* [pairfq](https://github.com/sestaton/Pairfq) 0.17.0 (should be in $PATH, it is called in the scripts as `pairfq`)


## Preprocessing

###	Introduction
This pipeline describes how to analyze GBS data of *Hymenoscyphus fraxineus*. The double digest GBS libraries were prepared using an adapted protocol of (Elshire et al., 2011) and (Poland et al., 2012), with restriction enzymes PstI and HpaII (= MspI isoschizomer). Resulting libraries were paired end sequenced (2x150 bp). This data analysis pipeline is specifically tailored for this protocol, i.e. double digest GBS and paired end sequencing. It basically consists of two major steps:
* 1) Preprocessing of the data
* 2) Reference-free locus identification and comparison

The preprocessing of the data is done with custom scripts which were based on the single digest GBS data analysis workflow of Christophe Verwimp (GBS of ryegrass and comb jelly). The reference-free locus identification is mainly done using the software GIbPSs (Hapke and Thiele, 2016).
The preprocessing of the GBS data consists of 4 steps:
* 1) Demultiplexing
* 2) Trimming of adapters and restriction site remnants
* 3) Merging of F and R reads
* 4) Quality filtering
The preprocessing scripts are structured as follows. There are main scripts (`GBS_Demultiplexing_v2.sh`, `GBS_Trimming_v7_PE.sh`, `GBS_Merging_v1.sh`, `GBS_QualityFiltering_v7.sh`, `FastQ_CountReads.py`, `FastQ_TrimInternalRestrictionSites.py`) which do the actual processing, these scripts should be placed together in a single folder. Next to the main scripts, there are so-called Wrapper scripts: these scripts refer to the main scripts and contain all parameters necessary to run the analysis (`WrapperScriptDemultiplexing.sh`, `WrapperScriptTrimming.sh`, `WrapperScriptMerging.sh`, `WrapperScriptQualityFiltering.sh`). You have to open the Wrapper scripts in your favorite text editor (for example [Notepad++](https://notepad-plus-plus.org/)) and adjust the parameters as explained below.

###	Create directory tree and prepare input files
To nicely structure your data, it is highly recommended to create a number of directories for the different steps in the preprocessing. You can for example make a directory “Preprocessing” which contains the subdirectories “Scripts”, “Demultiplexing”, “Trimming”, “Merging”, “QualityFiltering”. Each of these folders (except “Scripts”) can have a subfolder “Log” and a subfolder “Output”.
To easily create all these folders in a single step, execute the following command:
```
mkdir –p Preprocessing/{Scripts,Demultiplexing/{Log,Output},QualityFiltering/{Log,Output},Trimming/{Log,Output},Merging/{Log,Output}}
```
Next, copy all scripts to the “Scripts” folder.
Finally, we need to prepare some input files which are needed for the different scripts: a `TableOfBarcodes.txt` file and a `ListOfSamples.txt` file.
The `TableOfBarcodes.txt` file is a tab separated file containing the following information:
```
column 1	sample name
column 2	barcode sequence
column 3	restriction enzyme (compatible to barcode adapter)
column 4	optionally, second restriction enzyme
column 5	optionally, second barcode sequence
```
The `ListOfSamples.txt` file is a file which contains 1 column with on each line the name of the sample which needs to be included in the analysis.

If you created these files in Windows, do not forget to convert them to Unix format. This can be done from the command line using [dos2unix](http://dos2unix.sourceforge.net/) as follows:
```
dos2unix TableOfBarcodes.txt
dos2unix ListOfSamples.txt
```

###	Demultiplexing
The first step of the preprocessing consists of demultiplexing of the raw data, i.e. splitting the reads according to the sample from which they were derived. You can allow a maximum number of mismatches to the barcode. The barcode is subsequently clipped off and the reads are put in separate files according to the sample.
Open the `WrapperScriptDemultiplexing.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the demultiplexed files
LogDirectory= location where the program should put a log file
RawDataForward= location of the fastq file with forward reads
RawDataReverse= location of the fastq file with reverse reads
TableOfBarcodes= TableOfBarcodes.txt file
NumberOfMismatchesAllowed= maximum number of allowed mismatches in the barcode
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Log"
RawDataForward="/home/genomics/ALL_RAW_SEQ_DATA/GBS/Resipath/forward_reads.fq"
RawDataReverse="/home/genomics/ALL_RAW_SEQ_DATA/GBS/Resipath/reverse_reads.fq"
TableOfBarcodes="/home/genomics/kris/1_Preprocessing/TableOfBarcodes.txt"
NumberOfMismatchesAllowed=1
```
Next, run the Demultiplexing script as follows:
```
bash WrapperScriptDemultiplexing.sh
```
The input `TableOfBarcodes.txt` is ordered on descending barcode length to avoid wrong assignment when using high numbers of allowed mismatches. The raw fastq files are read and demultiplexed using the software [GBSX](https://github.com/GenomicsCoreLeuven/GBSX) (Herten et al., 2015). An F for 'forward' and R for 'reverse' is added to the names of sample specific FastQ files. Undetermined reads are stored in a separate FastQ file. A new file is created (e.g. RawData_output.tbl), containing the sample names and the numbers of reads per sample after demultiplexing. Samples with low numbers of reads should be excluded for further processing.

IMPORTANT NOTE: in the `GBS_Demultiplexing_v2.sh` script (line 66), you should change the path to GBSX to the path on your system where GBSX is found.

### Trimming
After the demultiplexing, the reads are trimmed. This is done in two steps. First the adapter at the 3’ end is cut off including the remainder of the restriction site. Next, the remainder of the restriction site at the 5’ end is clipped off. The script uses both the cutadapt (Martin, 2011) and [FastX](http://hannonlab.cshl.edu/fastx_toolkit/index.html) toolkit software. It also works in parallel to go faster using [GNU parallel](https://www.gnu.org/software/parallel/).
Open the `WrapperScriptTrimming.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
LogDirectory= location where the program should put a log file
OutputDirectory= location where the program should put the trimmed files
InputDirectory= location of the input files (output of demultiplexing)
TableOfBarcodes= TableOfBarcodes.txt file
Sequencing= “SE” for single end sequencing and “PE” for paired end sequencing
ReadLength= exact length of the input reads
MinimumLength= minimum length of reads to keep, put this at min. 10 to avoid broken pairs later on
MaxNumberOfCores= number of processors the program can use
DeleteIntermediateFiles= whether you want to delete intermediate files ("FALSE" or “TRUE”)
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing_bis/Scripts"
LogDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Log"
OutputDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Output"
InputDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Output"
TableOfBarcodes="/home/genomics/kris/1_Preprocessing_bis/TableOfBarcodes.txt"
Sequencing="PE"
ReadLength=151
MinimumLength=10
MaxNumberOfCores=16
DeleteIntermediateFiles="FALSE"
```
Next, run the Trimming script as follows:
```
bash WrapperScriptTrimming.sh
```

###	Merging
If the size of the fragments was relatively low, there is a great chance that forward and reverse reads can overlap. In order to avoid having two separate loci (one derived from the F read and one from the R read) that are in reality only one locus, it is best to check if F and R reads are overlapping and can be merged. In this step we check whether that is the case or not using the software [PEAR](https://cme.h-its.org/exelixis/web/software/pear/) (Zhang et al., 2014). The software takes into account the quality of the bases, in case there is a mismatch in the overlap, it will select the base with the highest quality and it will adjust the quality values accordingly. If reads can be merged, they are added to a new file with merged reads. If they cannot be merged, they remain in two separate files containing F and R reads.
Open the `WrapperScriptMerging.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the merged files
LogDirectory= location where the program should put a log file
InputDirectory= location of the input files (output of trimming)
ListOfSamples= ListOfSamples.txt file
MinOverlap= minimum overlap needed between F and R read in order to start merging
MaxNumberOfCores= number of processors the program can use
MaxMemory= maximum amount of memory the program can use (e.g. “30G” for 30 GB RAM)
MinLength= minimum length of merged reads (smaller merged fragments are discarded)
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing_bis/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing_bis/Merging/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing_bis/Merging/Log"
InputDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Output"
ListOfSamples="/home/genomics/kris/1_Preprocessing_bis/ListOfSamples.txt"
MinOverlap=10
MaxNumberOfCores=16
MaxMemory="30G"
MinLength=25
```
Next, run the Merging script as follows:
```
bash WrapperScriptMerging.sh
```

###	Quality Filtering
The final step in the preprocessing is the quality filtering. This script does the following things:
* Bases with quality value lower than x are replaced by N. If you do not want this substitution, just use a very low quality threshold (e.g. Q<1).
* An extra trimming step is done of x bases at the 3’ end of the remaining paired reads (not the merged reads). This is because the last part of the sequence is typically of low quality.
* Sequences with a mean quality less than x are removed.
* Sequences shorter than x bp are removed.
* Sequences with more than x % N’s are removed

Of the quality filtered files, a copy is made where the reads that still contain internal restriction sites are removed.
To do all these steps, the script uses the software [FastX toolkit](http://hannonlab.cshl.edu/fastx_toolkit/index.html), [prinseq-lite](https://github.com/b-brankovics/grabb/blob/master/docker/prinseq-lite.pl) (Schmieder and Edwards, 2011), [obitools](https://pythonhosted.org/OBITools/welcome.html) (Boyer et al., 2015) and [pairfq](https://github.com/sestaton/Pairfq). The script keeps the merged files, the remaining pairs and the resulting singletons after filtering in separate files.
Open the `WrapperScriptQualityFiltering.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the quality filtered files
LogDirectory= location where the program should put a log file
InputDirectory= location of the input files (output of merging)
ListOfSamples= ListOfSamples.txt file
MinMeanQuality= Minimum mean quality a read should have
MinLength= Minimum length a read should have
MinBaseQuality= A base with a lower quality of this is converted into an N
MaxNumberOfNs= Maximum percentage of N’s allowed. If more than x % of the bases are N’s, the read is removed.
MaxNumberOfCores= number of processors the program can use
TrimmingLengthOfNonMergedReads= number of bases that can be trimmed of the 3’ part of non merged reads
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing/QualityFiltering/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing/QualityFiltering/Log"
InputDirectory="/home/genomics/kris/1_Preprocessing/Merging/Output"
ListOfSamples="/home/genomics/kris/1_Preprocessing/ListOfSamples.txt"
MinMeanQuality=25
MinLength=30
MinBaseQuality=1
MaxNumberOfNs=10
MaxNumberOfCores=16
TrimmingLengthOfNonMergedReads=30
```
Next, run the QualityFiltering script as follows:
```
bash WrapperScriptQualityFiltering.sh
```

The output folder will contain the following files:
* A quality filtered file with merged reads
* Two quality filtered files with read pairs (file with F read and file with R reads)
* A quality filtered file with singleton reads
* A copy of the four previous files where the reads with internal restriction sites were removed.

Also an output table is generated with the remaining number of reads in each file for each sample.


## References

Boyer, F., Mercier, C., Bonin, A., Le Bras, Y., Taberlet, P., Coissac, E., 2016. OBITools: a Unix-inspired software package for DNA metabarcoding. Mol. Ecol. Resour. 16, 176-182.

Darriba, D., Taboada, G.L., Doallo, R., D., P., 2012. jModelTest 2: more models, new heuristics and parallel computing. Nat Methods 9, 772.

Elshire, R.J., Glaubitz, J.C., Sun, Q., Poland, J. a, Kawamoto, K., Buckler, E.S., Mitchell, S.E., 2011. A robust, simple genotyping-by-sequencing (GBS) approach for high diversity species. PLoS One 6, e19379.

Herten, K., Hestand, M.S., Vermeesch, J.R., Van Houdt, J.K., 2015. GBSX: a toolkit for experimental design and demultiplexing genotyping by sequencing experiments. BMC Bioinformatics 16, 1–6.

Martin, M., 2011. Cutadapt removes adapter sequences from high-throughput sequencing reads. EMBnet.journal 17, 10.

Poland, J. a, Brown, P.J., Sorrells, M.E., Jannink, J.-L., 2012. Development of high-density genetic maps for barley and wheat using a novel two-enzyme genotyping-by-sequencing approach. PLoS One 7, e32253.

Schmieder, R., Edwards, R., 2011. Quality control and preprocessing of metagenomic datasets. Bioinformatics 27, 863–864.

Zhang, J., Kobert, K., Flouri, T., Stamatakis, A., 2014. PEAR: A fast and accurate Illumina Paired-End reAd mergeR. Bioinformatics 30, 614–620.
