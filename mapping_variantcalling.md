# Reference-based analysis: mapping and variant calling

## Introduction
This document describes the analysis of Genotyping-by-Sequencing (GBS) loci using mapping against a genome, and subsequent variant (SNPs and indels) calling.
Requirements:
- preprocessed GBS data (demultiplexed, adapters & restriction enzyme remnants removed, quality filtered, merged)
- reference genome to map against

All scripts are distributed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation requirements
The following software needs to be installed on your system. The versions indicated are the versions on which the scripts were tested, it is possible that other versions work as well.
- [python v3.6.8](https://www.python.org/) with packages `os`,`sys`,`argparse`,`datetime`, `multiprocessing`
- [BWA v0.7.17-r1188](http://bio-bwa.sourceforge.net/), called in scripts as `bwa`
- [SAMtools v1.7](http://samtools.sourceforge.net/), called in scripts as `samtools`
- [Picardtools v2.8.1](https://broadinstitute.github.io/picard/), called in scripts as `PicardCommandLine <program>`
- [GATK v4.2.1.0](https://software.broadinstitute.org/gatk/), in our case installed as Docker, and called in scripts as `gatk` after launching the Docker (see below for more information).
- [vcftools v0.1.15](https://vcftools.github.io/index.html), called as `vcftools`
- [RStudio](https://www.rstudio.com/) with [R](https://www.r-project.org/) 3.6.3 including the following packages: dplyr, vcfR, ape, vegan, gplots, RColorBrewer, adegenet, poppr, pegas, ade4, factoextra, SNPRelate, igraph, ggrepel, ggtree, phytools

If any of the above software is installed differently on your system, you should adjust the way the software is called in the scripts.


## Preparation of reference genome
The fasta file of the reference genome needs to be downloaded and index files need to be made (necessary for different subsequent programs).
```
bwa index genome.fasta
samtools faidx genome.fasta
PicardCommandLine CreateSequenceDictionary R=genome.fasta O=genome.dict
```

## Mapping
Next, the preprocessed GBS files can be mapped against the reference genome. We do this using a python script which calls `BWA` in parallel to be able to process multiple input files at once. In this case, we use only the "merged" reads (overlapped forward and reverse reads), so we use the script in `--single` mode.

"Read groups" are also added to the mapping files. Read groups define the library, sample and platform unit (lane) on the Illumina machine. Therefore it is recommended to do this mapping step in batches of read groups, typically this means that you will map all samples from the same lane (and the same library type) in one batch. For the read groups, a read group ID should be given (which is in our case the sample ID), as well as a library batch, and the platform unit (lane of the machine). More information on read groups and why it is important to add this information can be found [here](https://software.broadinstitute.org/gatk/documentation/article.php?id=6472).

The script uses `BWA mem` to do the mapping, `samtools` to convert the resulting mapping files to sorted bam files and `Picardtools` to add read group information.

The script is used as follows:

`python3 Map_BWA_parallel.py <options> genome.fasta`

Mandatory positional argument: Reference genome in fasta format

Options:
- `--single` : use single reads as input
- `--paired` : use paired reads as input (default)
- `-d` or `--dir`: directory where the input files are in fastq format. Default is current directory. The directory path should end with a "/"
- `-p` or `--processes` : number of processors to use in parallel
- `-l` or `--log` : name of the log file (default: MAP_BWA_parallel.log)
- `-s` or `--suffix` : suffix of the fastq files (default: .fq)
- `-b` or `--lib` : read group library (default: libx)
- `-u` or `--pu` : read group platform unit (default lanex)

Example (we use merged reads, so we use the `--single` option):
`python3 Map_BWA_parallel.py -p 8 --single -d /home/genomics/ahaegeman/GBS_Hymenoscyphus/0_input_preprocessed_data/ --lib lib1 --pu lane1 /home/genomics/ahaegeman/GBS_Hymenoscyphus/Genome/genome.fasta`

The output BAM files will be stored in the folder from where you run the command.

After the mapping, the number of mapped reads for each sample can be counted as follows:
```
for b in *.bam; do total=`samtools view -c "$b"`; mapped=`samtools view -c -F 4 "$b"`; unmapped=`samtools view -c -f 4 "$b"`; echo -e "$b\t$total\t$mapped\t$unmapped" >> allsamples.readcounts.txt; done
```
A file with a list of the resulting bam files (needed for the Variant calling script) can be made as follows:

`cut -f1 allsamples.readcounts.txt > bamfiles.list`

Based on the number of mapped reads, you may decide to remove some samples with too little mapped reads from the `bamfiles.list`. Please note that the list of bam files should be solely the BAM file name without the path.

## Variant calling
In the next step, the variant calling is done using GATK's HaplotypeCaller.

Since we use `GATK` as a docker, we first need to launch the docker, and mount our data folder containing the mapping files, in this case `/home/genomics/ahaegeman/GBS_Hymenoscyphus/1_Mapping`.

`docker run -v /home/genomics/ahaegeman/GBS_Hymenoscyphus/1_Mapping:/gatk/mydata -it broadinstitute/gatk`

When the docker is launched, we navigate to the mounted data folder:
`cd /gatk/mydata`

Using `ls` you can check if your mapping files can be found there.
We see that the genome reference file is located in another directory and hence will not be found by the GATK docker. Therefore we copy the `genome.fasta` as well as the `genome.dict` and `genome.fai` file to the folder containing the mapping files. Similarly, we also need to copy the `VariantCalling_GATK_HaplotypeCaller.py` script to the folder containing the mapping files.


Next, the python wrapper script calling GATK's HaplotypeCaller is run as follows:

`python3 VariantCalling_GATK_HaplotypeCaller.py <options> bamfiles.list genome.fasta output.vcf`

Mandatory positional arguments (in the following order):
- text file with list of input BAM files (1 per line), with suffix .list
- reference genome (fasta format)
- name of output VCF file

Options:
- `-L` or `--positions`: BED file containing list of regions where the variants should be called (default: none)
- `-p` or `--processes`: number of processors to use to run the script in parallel (default: 4)
- `-n` or `--ploidy` : the ploidy level for the variant calling (default: 2)

Example:
`python3 VariantCalling_GATK_HaplotypeCaller.py bamfiles.list genome.fasta myvariants.vcf -p 6 -n 1`

Exit the GATK docker:
`exit`

The resulting vcf, gvcf and log files are now in the folder with the BAM files from which we ran GATK. You can move all resulting files to another folder as follows:
`mv *.vcf /destination/folder`
`mv *.vcf.* /destination/folder`

You can also remove the copy of the script and the copy of the reference genome file including index files from the folder, since we are done using `GATK`.

## Variant filtering

After the variant calling, it is important to do proper variant filtering to obtain high quality variants and to get rid of noise. The software `vcftools` is a perfect tool to do the variant filtering. Settings depend on the database, but some suggested filtering settings are given below.

`vcftools --vcf inputfile.vcf --out outputfileprefix <options>`

The most interesting options for variant filtering are listed below. It is recommended to do two rounds of filtering.
- `--minGQ` : Exclude all genotypes with a quality below the threshold specified. The Genotype Quality (GQ) is a score given by the variant calling algorithm, for more information, see the GATK website.
- `--minDP` : Exclude all genotypes with a read depth below the threshold specified.

First you can do "genotype filtering", which means you can put individual genotype calls for individual samples as missing data. This is interesting in case some individuals performed more poorly in the previous analyses (for example less reads).

After "genotype filtering" you can start filtering variants to keep only high quality SNPs and/or indels.
- `--recode` : output as VCF file
- `--recode-INFO-all` : keep all INFO fields in the output
- `--minQ` : keep only sites with a minimum quality of Q
- `--max-missing` : Exclude sites on the basis of the proportion of missing data (defined to be between 0 and 1, where 0 allows sites that are completely missing in all individuals and 1 indicates no missing data allowed)
- `--maf` : Include only sites with a Minor Allele Frequency greater than or equal to the `--maf` value and less than or equal to the `--max-maf` value. Allele frequency is defined as the number of times an allele appears over all individuals at that site, divided by the total number of non-missing alleles at that site.
- `--min-alleles` : Include only sites with a number of alleles greater than or equal to the "--min-alleles" value
- `--max-alleles` : Include only sites with a number of alleles less than or equal to the "--max-alleles" value
- `--min-meanDP` : Include only sites with mean depth values (over all included individuals) greater than or equal to the "--min-meanDP" value
- `--keep-only-indels` : keep only indels and remove SNPs
- `--remove-indels` : remove indels and keep SNPs only
- `--indv` : sample name of sample/individual to keep. Use the option multiple times to keep multiple individuals.


**Example**:

We first do genotype filtering where we require a minimum read depth of 6 and a minimum genotype quality of 30 for each called variant. All genotypes that do not meet these thresholds are recoded as missing data.

`vcftools --vcf myvariants.vcf --out myvariants.GTfiltered --recode --recode-INFO-all --minGQ 30 --minDP 6`

Next we keep only variant sites with less than 20% of the individuals with missing data, with maximum 2 alleles, with a minimum quality of 20 and with a minimum mean depth of 10.

`vcftools --vcf myvariants.GTfiltered.vcf --out myvariants.GTandsitefiltered --recode --recode-INFO-all --max-missing 0.80 --max-alleles 2 --min-meanDP 10 --minQ20`

**Remark on prefiltering**: depending on the subsequent analysis, the first step of genotype filtering can be omitted. For example if you plan to calculate the Hamming distance (to make a distance tree), it is better not to do prefiltering of the genotypes. Hamming distance looks at similar characters between samples, and by doing prefiltering, you will put individual genotype values to NA (missing). This would create artefacts because the missing sites will then be considered distance 0 according to Hamming. In this case it makes more sense to leave the genotype calls as they are (even though of lower quality), because it is better to have some value than to have a missing value.

In case of the Hymenoscyphus dataset, we decided to omit the prefiltering step, and to filter with maximum 2 alleles and aminimum depth of 10.
`vcftools --vcf Hymenoscyphus_variants.vcf --recode --recode-INFO-all --max-alleles 2 --min-meanDP 10 --out Hymenoscyphus_variants_maxalleles2_minmeanDP10`

## Downstream analysis

The resulting VCF file can now be used for various purposes. In our case, we read the VCF file into R and did several analyses there, partially inspired by [this](https://grunwaldlab.github.io/Population_Genetics_in_R/gbs_analysis.html) tutorial from the Grünwald lab.
The R script `analyze_vcf.R` reads the VCF file and also a metadata file with extra information on the samples. The `R` script can be opened in `Rstudio` and should be run from there. Make sure that the metadata file has a column which contains the same sample ID as in the VCF file.

Some analyses which are covered in the R script are:
- Distance calculation based on Hamming distance and on "Identity-by-state" (IBS)
- Heatmap of IBS values
- Principal Component Analysis
- Hierarchical clustering (average) = UPGMA, including calculating multiple UPGMA trees based on a subset of the variants
