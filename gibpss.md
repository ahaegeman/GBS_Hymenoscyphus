# Reference-free analysis with GIbPSs

This repository contains scripts for the processing of Genotyping-by-Sequencing data from *Hymenoscyphus fraxineus* isolates. This document describes how to use some scripts for downstream analyses after analysis with [Gibpss](https://github.com/ahapke/gibpss) (reference-free GBS analysis software) (Hapke et al., 2016).
Important remark: the scripts were made for our local server and may need some updating before they will run on your system (for example absolute paths to external software should be updated).

All scripts are distributed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation requirements

The following software was used to develop and run the scripts in this repository (newer/older versions of the software might work as well, but this was not tested).

* Operating system: Ubuntu 18.04
* [perl](https://www.perl.org/) 5.26.1
* [Bioperl](https://bioperl.org/) 1.7.7
* [Gibpss](https://github.com/ahapke/gibpss) (Hapke et al., 2016)
* [RStudio](https://www.rstudio.com/) with [R](https://www.r-project.org/) 3.6.3 including the following packages: data.table, ggplot2, vegan, fastcluster, pvclust, factoextra, cluster, ggdendro, phangorn, latticeExtra, ggrepel, stringr
* [fasttree](http://microbesonline.org/fasttree/) 2.1.11 (Price et al., 2010)



## GIbPSs

A reference-free analysis of the preprocessed reads is done using the software [Gibpss](https://github.com/ahapke/gibpss) (Hapke et al., 2016). We strongly recommend to read the extensive manual on the website to be able to set the parameters to your needs. Below is a concise overview of the different GIbPSs commands we used in this workflow.

### Locus identification per individual using `indloc`

`indloc_23.0.pl -i infiles.txt -d ind_distance_settings.txt -D 5 -mdl 10 -P 16 -r 0 -M f -c 0.2  -a 0.2`

With the following options:
`-i` : file containing the input fastq files
`-d` : file containing the distance settings
`-D` : minimum depth of good sequence variants, e.g. 5
`-mdl` : minimum depth of a locus, e.g. 10
`-P` : number of processors to use, e.g. 16 (check with command top how many processors are in use. If many in use, set to 8; if none or only a few are in use, set to 24 (there are 32 processors in total))
`-r 0` : do not build reverse complement duplicates
`-M` : method for SNP calling, f: frequency threshold method
`-c` character frequency threshold, e.g. 0.2
`-a` allele frequency threshold, e.g. 0.2


### Comparing loci between individuals using `poploc`

`poploc_09.0.pl -msl 0 -d pop_distance_settings.txt`

With the following options:
`-msl 0` : inactivate merging of overlapping fragments (we already merged during the preprocessing)
`-d` : file containing the distance settings

### Make link between indloc and poploc loci using `indpoploc`

`indpoploc_06.0.pl`

### Length selection using `data_selector`

`data_selector_16.0.pl`

Select (2) select loci, then (3) sequence length and then set the minimum length to 32 and the maximum to 300  bp. Then choose 13 to run data selector.

### Check for indels using `indelchecker`

`indel_checker_03.0.pl -UP /usr/local/bin/usearch -DP /home/genomics/ahaegeman/Run1/gibpss`

It has two main options:
`-UP` absolute path to usearch executable
`-DP` absolute path to your working directory with your loci

### Remove indels using `data_selector`

`data_selector_16.0.pl`

Select loci (2), List of loci in a file (2): ./indelcheck/no_indel_loc.txt (beware to write this correctly and check whether the program accepted the file and that the selection of loci is ok). Run dataselector by selecting 13.

### Depth analysis using `depth_analyzer`

`depth_analyzer_09.0.pl lengths_for_depth_analyzer.txt`

With `lengths_for_depth_analyzer.txt` consisting of 1 line only in this case:
`32 300`

Subsequently, select loci based on the depth analysis by running “data_selector” (see previous point). To determine the cutoffs at which you want to select loci, we typically generate graphs in R.

### Remove split loci using `data_selector`

`data_selector_16.0.pl`

Select (2) select loci, then (9) split loci, then (0) no split loci, then (13) Run.

### Export genotype tables using `export_tables`

`export_tables_07.0.pl`

The script produces four output files in the “export” directory:
-	genotypes.txt: this is the most important output file with all genotypes for all loci in all individuals.
-	reads.txt: this file contains the read depths per locus for each individual
-	locdatasel.txt: this file contains properties of the selected loci (such as locus length, number of variable positions, number of individuals that have this locus, etc.)
-	locdata.txt: this file is similar to locdatasel.txt, but contains information on the loci of the complete database, not just the subselection.


## Parent hybrid comparison

The `parent_hybrid_comparison.py` script compares all isolates to each other. It uses the `genotypes.txt` file as generated  by the Gibpss software. Two things are calculated:
1) the number of loci in common : this gives the absolute number of loci that occur in both isolates.
2) the allele similarity (in %) : this gives the percentage of loci that two isolates have in common, that share at least 1 allele.
These two values are given for all one-to-one isolate comparisons in the form of similarity tables: `commonloci.txt` and `snpsim.txt`.

Example: isolate1 and isolate2 have 20.000 loci in common and have an allele similarity of 5.0%. This means that 1000 loci (5% of 20.000) have an allele which occurs in both isolates, without any SNPs. If there is at least 1 nucleotide difference between the alleles, the corresponding locus is not counted in the allele similarity.

The script can be run as follows:

`python3 parent_hybrid_comparison.py genotypes.txt`

## Hierarchical clustering

The `hierarchical_clustering_presence_absence.R`script is an R script that reads the `genotypes.txt` output file from Gibpss. It will then convert the file to a presence/absence object (1/0), and based on this data, a hierarchical clustering analysis (UPGMA) is done by using the Jaccard distance.
The `R` script can be opened in `Rstudio` and should be run from there.

## Concatenated alignment and phylogenetic tree

From the Gibpss output files `genotypes.txt` and `locdata.txt`, we can create a concatenated alignment in fasta format, concatenating all the loci from the different isolates using the script `convert_genotypes_to_concatenated_alignment_and_filter.pl`.

The script will convert the Gibpss genotypes.txt file to a concatenated alignment in fasta format. It will also do an additional filtering step to remove: 1) invariant positions (positions which have the same base across all individuals, gaps are considered as a base); 2) positions with maximum <g> gaps, i.e. maximum <g> individuals with missing data. The script also takes the locdata.txt file from Gibpss as input to keep track of which loci and bases are kept in the final alignment. In the example below, we do nAn output file containing this information (called "info_positions_filtered_alignment.txt") is generated automatically when running the script.

You can run it as follows (in the example below we allow max. 4 individuals with missing data):

`perl convert_genotypes_to_concatenated_alignment_and_filter.pl -i genotypes.txt -l locdata.txt -g 4 -o concatenated_alignment.fasta`


In case you want to select certain sites from a concatenated alignment, you can use the script `select_sites_from_filtered_alignment.pl`.
The script will filter a concatenated alignment based on a list of positions to keep. It also reads the "info_positions_filtered_alignment.txt" file from the concatenated alignment as generated by the `convert_genotypes_to_concatenated_alignment_and_filter.pl` script.
The output is a filtered fasta file and a new "info_positions_selected.txt" file is created containing only the sites kept.

You can run it as follows, withe `sites.txt` a file with the positions of the sites to be kept (one on each line):
`perl select_sites_from_filtered_alignment.pl -i concatenated_alignment.fasta -p info_positions_filtered_alignment.txt -s sites.txt -o concatenated_alignment_selectedsites.fasta`

Finally, you can use one of the alignments created above to build a phylogenetic tree. An easy way to do this is using [fasttree](http://microbesonline.org/fasttree/) (maximum likelihood). The output is a tree in Newick format.

`fasttree -nt < concatenated_alignment.fasta > concatenated_alignment_MLfasttree.new`

The tree can be visualized with your preferred software. For example using [iTOL](https://itol.embl.de/upload.cgi).

## References

Hapke, A., Thiele, D., 2016. GIbPSs: a toolkit for fast and accurate analyses of genotyping-by-sequencing data without a reference genome. Mol. Ecol. Resour. 16, 979-990.

Price, M.N., Dehal, P.S., and Arkin, A.P., 2010. FastTree 2 -- Approximately Maximum-Likelihood Trees for Large Alignments. PLoS ONE, 5(3):e9490. doi:10.1371/journal.pone.0009490.
