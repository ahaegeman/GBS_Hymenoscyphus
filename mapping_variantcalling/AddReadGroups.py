#!/usr/bin/python3

#===============================================================================
# Description
#===============================================================================

# Elisabeth VEECKMAN November 2016
# Modified by Annelies Haegeman September 2019

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime
import multiprocessing

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Improve read mapping files in parallel.')
parser.add_argument('reference',
                    help = 'Full path and name of the reference genome')

# Add optional arguments
parser.add_argument('-d', '--dir',
                    default = '.',
                    type = str,
                    help = 'Directory containing BAM files (default current directory)')
parser.add_argument('-s', '--suffix',
                    default = '.bam',
                    type = str,
                    help = 'Suffix of BAM files to take into account for mapping (default .bam)')
parser.add_argument('-p', '--processes',
                    default = 4,
                    type = int,
                    help = 'Define the number of parallel processes (default 4)')

# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    """
    Print the current date and time to stderr.
    """
    sys.stderr.write('----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return

def commands (basename, ref = args['reference'], dir = args['dir'], suffix = args['suffix']):
    """
    This function contains all commands needed to improve the reads mapped in each BAM file.
    The different steps are:
    - Add read group info, based on sample name (first part of the basename, split on .)
    - Mark duplicates
    - Indel Realignment using GATK Realigner, to reduce the number of miscalls of INDELs
    Intermediary BAM files are indexed. All intermediary files are removed at the end of the function.
    """
    
    sample = basename.split('.')[0]
    
    if not os.path.isfile(basename + '.realigned.bam'):
        print(basename)

        # add read group information
        cmd1A = 'PicardCommandLine AddOrReplaceReadGroups INPUT=' + dir + '/' + basename + suffix  + ' OUTPUT=' + basename + '.rg.bam RGID=group1 RGSM=' + sample + ' RGPL=illumina RGLB=libx RGPU=lanex 2>> ' + basename + '.rg.log'
        os.system(cmd1A)
        cmd1B = 'samtools index ' + basename + '.rg.bam'
        os.system(cmd1B)
    
        # mark duplicates
#        cmd2A = 'java -jar /usr/local/bioinf/picard-tools-1.113/picard-tools-1.113/MarkDuplicates.jar MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 INPUT=' + basename + '.rg.bam OUTPUT=' + basename + '.dedup.bam METRICS_FILE=' + basename + '.dedup.metrics  2>> ' + basename + '.dedup.log'
#         os.system(cmd2A)
#        cmd2B = 'samtools index ' + basename + '.dedup.bam'
#         os.system(cmd2B)
         
#         # indel Realignment
# #         cmd3A = 'java -Xmx2G -jar /usr/local/bioinf/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T RealignerTargetCreator -R ' + ref + ' -I ' + basename + '.rg.bam -o ' + basename + '.IndelRealigner.intervals --fix_misencoded_quality_scores -fixMisencodedQuals 2>> ' + basename + '.IndelRealign.log'
#        cmd3A = 'docker run broadinstitute/gatk gatk RealignerTargetCreator -R ' + ref + ' -I ' + basename + '.rg.bam -o ' + basename + '.IndelRealigner.intervals 2>> ' + basename + '.IndelRealign.log'
#        os.system(cmd3A)
# # #         cmd3B = 'java -Xmx4G -jar /usr/local/bioinf/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T IndelRealigner -R ' + ref + ' -I ' + basename + '.rg.bam -targetIntervals ' + basename + '.IndelRealigner.intervals -o ' + basename + '.realigned.bam --fix_misencoded_quality_scores -fixMisencodedQuals 2>> ' + basename + '.IndelRealign.log'
#        cmd3B = 'docker run broadinstitute/gatk gatk IndelRealigner -R ' + ref + ' -I ' + basename + '.rg.bam -targetIntervals ' + basename + '.IndelRealigner.intervals -o ' + basename + '.realigned.bam 2>> ' + basename + '.IndelRealign.log'
#        os.system(cmd3B)
#        cmd3C = 'mv  ' + basename + '.realigned.bai '+ basename + '.realigned.bam.bai'
#        os.system(cmd3C)
     
        # remove intermediary files
#         os.system('rm ' + basename + '.rg.bam')
#         os.system('rm ' + basename + '.rg.bam.bai')
#         os.system('rm ' + basename + '.dedup.bam')
#         os.system('rm ' + basename + '.dedup.bam.bai')
#         os.system('rm ' + basename + '.dedup.metrics')
#         os.system('rm ' + basename + '.IndelRealigner.intervals')

    return    

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    
    print_date()
    
    sys.stderr.write('* Improving read mapping: AddOrReplaceReadGroups, MarkDuplicates and IndelRealigner ...\n')
     
    # creating a pool of 4 workers
    with multiprocessing.Pool(args['processes']) as p:
         
        # build a list of tasks (= all names of files ending with suffix)
        tasks = [f[:(len(args['suffix'])  * -1)] for f in os.listdir(args['dir']) if f.endswith(args['suffix'])]
        # run tasks in parallel
        p.map(commands, tasks)
         
    sys.stderr.write('* Finished\n')
    
    print_date()

